## --- --- --- --- --- ---
#
# Purpose: Export data functions
#
# Author: Michael Miles
#
# Date Created: 2023-02-20
#  
## --- --- --- --- --- ---


# Save xlsx files --------------------------------------------------------------

# Saves named list of dataframes as sheets in an xlsx workbook
#' @export
save_xlsx <- function(df_list, save_path, file_name) {
  
  box::use(openxlsx)
  
  # Make file name for saving
  save_file <- paste0(save_path, file_name)
  
  # Save file
  openxlsx$write.xlsx(df_list,
                      file = save_file)
  
}
