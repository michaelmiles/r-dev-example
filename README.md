# Demo Script

## About

This script is for a Data Science Challenge.

Raw data is saved in io/input/
Output data is saved in io/output/data_demo.xslx with two sheets: 1) "device_by_month", 2) "month_over_month"

## Running the script

To run the script, open data_demo.Rproj and the run.R script. Click "Source" to run the script and generate the output file

## Project structure

Except for run.R, which is stored in the main repo, all code is stored in the R folder. Code is divide into the main script (R/main.R) and submodules that
are found in R/box/.

Sub-modules include:
- global_options / global_options.R (Sets basic script parameters, like local file directories)
- io / import.R (Functions for loading data)
- io / export.R (Functions for saving data)
- mod / mod.R (Functions for modifying data)

These submodules contain functions that are loaded in the main script and used to conveniently wrap complex code into easy to understand function names.